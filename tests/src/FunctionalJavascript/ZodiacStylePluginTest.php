<?php

namespace Drupal\Tests\zodiac_style_plugin\FunctionalJavascript;

use Drupal\entity_test\Entity\EntityTest;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\views\Tests\ViewTestData;

/**
 * Tests the carousel block.
 *
 * @group zodiac_style_plugin
 */
class ZodiacStylePluginTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'views',
    'views_ui',
    'user',
    'zodiac_style_plugin_views_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable9';

  /**
   * {@inheritdoc}
   */
  public static $testViews = ['zodiac_style_plugin_views_test'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    ViewTestData::createTestViews(self::class, ['zodiac_style_plugin_views_test']);

    $this->drupalLogin($this->drupalCreateUser([
      'administer views',
    ]));

    $slider_item_count = 25;
    for ($i = 0; $i < $slider_item_count; $i++) {
      $entity = EntityTest::create();
      $entity->save();
    }
  }

  /**
   * Test that breakpoint options are added when selecting a new group.
   */
  public function testBreakpoint() {
    /** @var \Drupal\FunctionalJavascriptTests\JSWebAssert */
    $assert = $this->assertSession();
    $page = $this->getSession()->getPage();

    $this->drupalGet('/admin/structure/views/view/zodiac_style_plugin_views_test');

    // Open the style options dialog.
    $page->clickLink('Change settings for this format');
    $assert->assertWaitOnAjaxRequest();

    // Select the test breakpoint set.
    $page->selectFieldOption('Use Breakpoint Group', 'zodiac_style_plugin_views_test');

    // Open the options for the Large breakpoint.
    $page->pressButton('Large (test)');
    $option_keys = [
      'autoplay',
      'autoplaySpeed',
      'gap',
      'itemsPerView',
      'pauseOnHover',
      'transitionSpeed',
    ];

    // Check that each option is visible for that breakpoint.
    foreach ($option_keys as $option_key) {
      $element = $assert->elementExists('css', "[name='style_options[breakpoint_options][zodiac_style_plugin_views_test][lg][$option_key]']");
      $this->assertTrue($element->isVisible());
    }

    $gap_field_selector = 'style_options[breakpoint_options][zodiac_style_plugin_views_test][lg][gap]';
    $page->fillField($gap_field_selector, '10');

    // Apply the updated style plugin form.
    $page->findField($gap_field_selector)->submit();
    $assert->assertWaitOnAjaxRequest();

    // Re-open the form.
    $page->clickLink('Change settings for this format');
    $assert->assertWaitOnAjaxRequest();

    // Ensure the breakpoint group and modified breakpoint settings were saved.
    $assert->pageTextContains('Large (test)');
    $gap_field = $assert->fieldExists($gap_field_selector);
    $this->assertEquals('10', $gap_field->getValue());
  }

}
