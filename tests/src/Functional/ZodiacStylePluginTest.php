<?php

namespace Drupal\Tests\zodiac_style_plugin\Functional;

use Drupal\entity_test\Entity\EntityTest;
use Drupal\Tests\views\Functional\ViewTestBase;
use Drupal\views\Entity\View;
use Drupal\zodiac_style_plugin\Plugin\views\style\Zodiac;

/**
 * Tests the carousel block.
 *
 * @group zodiac_style_plugin
 */
class ZodiacStylePluginTest extends ViewTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable9';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'zodiac_style_plugin_views_test',
    'views',
    'views_ui',
  ];

  /**
   * The ID for the slider.
   *
   * @var string
   */
  protected $sliderId;

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE, $modules = ['zodiac_style_plugin_views_test']): void {
    parent::setUp($import_test_views, $modules);

    $this->enableViewsTestModule();
    $this->drupalLogin($this->drupalCreateUser([
      'administer views',
      'administer blocks',
    ]));

    $slider_item_count = 25;
    for ($i = 0; $i < $slider_item_count; $i++) {
      $entity = EntityTest::create();
      $entity->save();
    }

    $this->sliderId = '#' . Zodiac::BASE_HTML_ID;
  }

  /**
   * Test the style plugin.
   */
  public function testStylePlugin() {
    $this->drupalGet('zodiac-style-plugin-test');
    $this->assertSession()->elementExists('css', $this->sliderId);
  }

  /**
   * Test that the block is placeable with a unique ID.
   */
  public function testBlockPlacement() {
    $assert = $this->assertSession();

    $this->drupalGet('admin/structure/block');
    $this->clickLink('Place block');

    // Ensure that the test block appears.
    $assert->linkByHrefExists('/admin/structure/block/add/' . \urlencode('views_block:zodiac_style_plugin_views_test-block') . '/' . $this->defaultTheme);

    // Place two carousel blocks.
    $this->drupalPlaceBlock('views_block:zodiac_style_plugin_views_test-block');
    $this->drupalPlaceBlock('views_block:zodiac_style_plugin_views_test-block');

    $this->drupalGet('');

    // Check that there are two carousels on the page.
    $assert->elementsCount('css', '.zodiac', 2);

    // Check that their IDs are different.
    $assert->elementExists('css', $this->sliderId);
    $assert->elementExists('css', $this->sliderId . '--2');

    // Check that there are drupal settings for each slider.
    $settings = $this->getDrupalSettings();

    $this->assertNotEmpty($settings['zodiac'][Zodiac::BASE_HTML_ID]);
    $this->assertNotEmpty($settings['zodiac'][Zodiac::BASE_HTML_ID . '--2']);
  }

  /**
   * A data provider containing the default options in the view.
   */
  public function testDefaultOptionsProvider() {
    return [
      'Autoplay' => ['autoplay', TRUE],
      'Autoplay Speed' => ['autoplaySpeed', 5000],
      'Enable Live Region' => ['enableLiveRegion', TRUE],
      'Gap' => ['gap', 8],
      'Infinite Scrolling' => ['infiniteScrolling', TRUE],
      'Items Per View' => ['itemsPerView', 5],
      'Live Region Text' => [
        'liveRegionText',
        'Slide @position of @total @title',
      ],
      'Pause On Hover' => ['pauseOnHover', TRUE],
      'Transition Speed' => ['transitionSpeed', 500],
    ];
  }

  /**
   * Test that the default options are set in the `drupalSettings` JSON object.
   *
   * @dataProvider testDefaultOptionsProvider
   */
  public function testDefaultOptions($option, $value) {
    $this->drupalGet('zodiac-style-plugin-test');

    $settings = $this->getDrupalSettings();

    $this->assertNotEmpty($settings['zodiac'][Zodiac::BASE_HTML_ID]);
    $this->assertEquals($settings['zodiac'][Zodiac::BASE_HTML_ID][$option], $value);
  }

  /**
   * A data provider with modified options for the carousel.
   */
  public function testModifiedOptionsProvider() {
    return [
      'Autoplay' => ['autoplay', FALSE],
      'Autoplay Speed' => ['autoplaySpeed', 2000],
      'Enable Live Region' => ['enableLiveRegion', FALSE],
      'Gap' => ['gap', 3],
      'Infinite Scrolling' => ['infiniteScrolling', FALSE],
      'Items Per View' => ['itemsPerView', 2],
      'Live Region Text' => ['liveRegionText', 'test'],
      'Pause On Hover' => ['pauseOnHover', FALSE],
      'Transition Speed' => ['transitionSpeed', 600],
    ];
  }

  /**
   * Test that the options can be modified.
   *
   * @dataProvider testModifiedOptionsProvider
   */
  public function testModifiedOptions($option, $value) {
    $view = View::load('zodiac_style_plugin_views_test');
    $display = &$view->getDisplay('default');
    $display['display_options']['style']['options']['base_options'][$option] = $value;
    $view->save();

    $this->drupalGet('zodiac-style-plugin-test');

    $settings = $this->getDrupalSettings();

    $this->assertNotEmpty($settings['zodiac'][Zodiac::BASE_HTML_ID]);
    $this->assertEquals($settings['zodiac'][Zodiac::BASE_HTML_ID][$option], $value);
  }

  /**
   * Tests that options for individual breakpoints can be set.
   */
  public function testBreakpointsOptions() {
    $test_settings = [
      'autoplay' => FALSE,
      'autoplaySpeed' => 2000,
      'gap' => 3,
      'itemsPerView' => 2,
      'pauseOnHover' => FALSE,
      'transitionSpeed' => 600,
    ];

    $view = View::load('zodiac_style_plugin_views_test');
    $display = &$view->getDisplay('default');
    $display['display_options']['style']['options']['breakpoint_group'] = 'zodiac_style_plugin_views_test';
    $display['display_options']['style']['options']['breakpoint_options']['lg'] = $test_settings;
    $view->save();

    $this->drupalGet('zodiac-style-plugin-test');

    $media_query_options = $this->getDrupalSettings()['zodiac'][Zodiac::BASE_HTML_ID]['mediaQueryOptions'] ?? NULL;

    $this->assertIsArray($media_query_options);
    $this->assertArrayHasKey('(min-width: 992px)', $media_query_options);

    $breakpoint_settings = $media_query_options['(min-width: 992px)'];
    $expected_breakpoint_settings = [
      'autoplay' => FALSE,
      'autoplaySpeed' => 2000,
      'gap' => 3,
      'itemsPerView' => 2,
      'pauseOnHover' => FALSE,
      'transitionSpeed' => 600,
    ];

    $this->assertEquals($expected_breakpoint_settings, $breakpoint_settings);
  }

}
