/**
 * @file
 * Initializes all Zodiac sliders on the page.
 */

(({ behaviors }) => {
  behaviors.zodiac = {
    attach(context, drupalSettings) {
      // Iterate over each configured slider instance to initialize it.
      Object.entries(drupalSettings.zodiac).forEach((slider) => {
        const [id, settings] = slider;
        const selector = `#${id}`;

        // We use once() here to ensure that sliders aren't be reinitialized
        // (e.g., after AJAX or the BigPipe module is executed).
        once(id, selector, context).forEach(() => {
          new Zodiac(selector, settings).mount();
        });
      });
    },
  };
})(Drupal);
