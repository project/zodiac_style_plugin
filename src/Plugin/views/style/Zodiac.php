<?php

namespace Drupal\zodiac_style_plugin\Plugin\views\style;

use Drupal\breakpoint\BreakpointManagerInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Style plugin to render each item in a Zodiac slider.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "zodiac_style_plugin",
 *   title = @Translation("Zodiac"),
 *   help = @Translation("Displays content in a Zodiac slider."),
 *   theme = "views_view_zodiac",
 *   display_types = {"normal"},
 * )
 */
class Zodiac extends StylePluginBase implements ContainerFactoryPluginInterface {

  /**
   * The base HTML ID to use for slider produced by this plugin.
   *
   * @see ::getHtmlId()
   *   For more information about how HTML IDs are generated.
   *
   * @var string
   */
  const BASE_HTML_ID = 'zodiac';

  /**
   * The breakpoint manager.
   *
   * @var \Drupal\breakpoint\BreakpointManagerInterface
   */
  protected BreakpointManagerInterface $breakpointManager;

  /**
   * The HTML ID to use for this slider.
   *
   * @var string
   */
  protected string $htmlId;

  /**
   * {@inheritdoc}
   */
  protected $usesGrouping = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, BreakpointManagerInterface $breakpoint_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->breakpointManager = $breakpoint_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    // Add all breakpoint-customizable options to the form as base options.
    //
    // These options, while capable of being customized at each breakpoint, can
    // also be set globally if breakpoints are not used.
    $form['base_options'] = $this->getBaseOptionsForm($this->options['base_options'] ?? []);

    // Allow the user to determine which breakpoint group to use.
    //
    // The value of this setting determines which breakpoints appear on the form
    // for further configuration by the user.
    $form['breakpoint_group'] = [
      '#type' => 'select',
      '#title' => $this->t('Use Breakpoint Group'),
      '#description' => $this->t('Selecting a breakpoint group allows you to customize options at each breakpoint to provide greater flexibility.'),
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $this->options['breakpoint_group'] ?? '',
      '#options' => $this->breakpointManager->getGroups(),
    ];

    foreach (\array_keys($this->breakpointManager->getGroups()) as $breakpoint_group) {
      foreach ($this->breakpointManager->getBreakpointsByGroup($breakpoint_group) as $breakpoint_id => $breakpoint) {
        // Breakpoint IDs are prefixed by their group's machine name, followed
        // by a period. Since a period character cannot be used in configuration
        // keys, we must strip it here.
        $clean_breakpoint_id = \preg_replace('/^' . \preg_quote($breakpoint_group, '/') . '\\./', '', $breakpoint_id);

        // Make the breakpoint group part of the form hierarchy to avoid element
        // name collisions. This will later be removed when serializing
        // submitted form values to satisfy config schema.
        $breakpoint_options = &$form['breakpoint_options'][$breakpoint_group][$clean_breakpoint_id];
        $defaults = $this->options['breakpoint_options'][$clean_breakpoint_id] ?? [];

        $breakpoint_options = [
          '#type' => 'details',
          '#title' => $breakpoint->getLabel(),
          '#states' => [
            'visible' => [
              ":input[name='style_options[breakpoint_group]']" => [
                'value' => $breakpoint_group,
              ],
            ],
          ],
        ];

        $breakpoint_options += $this->getBreakpointOptionsForm($defaults, [
          'enabled' => [
            ":input[name='style_options[breakpoint_group]']" => [
              'value' => $breakpoint_group,
            ],
          ],
        ]);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('breakpoint.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defineOptions() {
    $options = parent::defineOptions();

    $options['base_options']['default'] = [
      'autoplay' => TRUE,
      'autoplaySpeed' => 5000,
      'enableLiveRegion' => TRUE,
      'gap' => 8,
      'infiniteScrolling' => TRUE,
      'itemsPerView' => 5,
      'liveRegionText' => 'Slide @position of @total @title',
      'pauseOnHover' => TRUE,
      'transitionSpeed' => 500,
    ];

    return $options;
  }

  /**
   * Builds the base options form elements for the plugin.
   *
   * @param array $defaults
   *   The default values for each option.
   *
   * @return array
   *   The base options form elements for the plugin.
   */
  protected function getBaseOptionsForm(array $defaults) {
    return [
      'autoplay' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Autoplay'),
        '#description' => $this->t('Whether or not the slider should autoplay.'),
        '#default_value' => !empty($defaults['autoplay']),
      ],
      'autoplaySpeed' => [
        '#type' => 'number',
        '#title' => $this->t('Autoplay Speed'),
        '#description' => $this->t('The amount of time (in milliseconds) to delay between automatically cycling an item.'),
        '#default_value' => $defaults['autoplaySpeed'] ?? NULL,
        '#required' => TRUE,
        '#step' => 100,
        '#min' => 1000,
      ],
      'enableLiveRegion' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable Live Region'),
        '#description' => $this->t('If enabled, a live region will be rendered to announce the current slide position and title.'),
        '#default_value' => !empty($defaults['enableLiveRegion']),
      ],
      'gap' => [
        '#type' => 'number',
        '#title' => $this->t('Gap'),
        '#description' => $this->t('The spacing between each slide in pixels.'),
        '#default_value' => $defaults['gap'] ?? NULL,
        '#required' => TRUE,
        '#min' => 0,
      ],
      'infiniteScrolling' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Infinite Scrolling'),
        '#description' => $this->t('Allows the slider to transition endlessly.'),
        '#default_value' => $defaults['infiniteScrolling'],
      ],
      'itemsPerView' => [
        '#type' => 'number',
        '#title' => $this->t('Items Per View'),
        '#description' => $this->t('The total number of columns to display in the slider.'),
        '#default_value' => $defaults['itemsPerView'] ?? NULL,
        '#required' => TRUE,
        '#min' => 1,
      ],
      'liveRegionText' => [
        '#type' => 'textfield',
        '#title' => $this->t('Live Region Text'),
        '#description' => $this->t('The text template used for the live region updates. <code>@position</code> is the index of the active slide. <code>@total</code> is the total number of slider items. <code>@title</code> is the text used for the title of the slider item. The title is derived from the <code>data-zodiac-live-region-title</code> attribute within or on an item.'),
        '#default_value' => $defaults['liveRegionText'] ?? NULL,
      ],
      'pauseOnHover' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Pause On Hover'),
        '#description' => $this->t('If enabled, the slider will pause autoplay while the user hovers over it.'),
        '#default_value' => !empty($defaults['pauseOnHover']),
      ],
      'transitionSpeed' => [
        '#type' => 'number',
        '#title' => $this->t('Transition Speed'),
        '#description' => $this->t('The amount of time (in milliseconds) it takes for the slider to animate to the next slide.'),
        '#default_value' => $defaults['transitionSpeed'] ?? NULL,
        '#required' => TRUE,
        '#step' => 100,
        '#min' => 100,
      ],
    ];
  }

  /**
   * Builds the breakpoint-customizable options form elements for the plugin.
   *
   * @param array $defaults
   *   The default values for each option.
   * @param array $states
   *   An optional array of form states to apply to each element (default: []).
   *
   * @return array
   *   The breakpoint-customizable options form elements for the plugin.
   */
  protected function getBreakpointOptionsForm(array $defaults, array $states = []) {
    $options = $this->getBaseOptionsForm($defaults);

    $options['autoplay'] = [
      '#type' => 'select',
      '#title' => $this->t('Autoplay'),
      '#description' => $this->t('Whether or not the slider should autoplay.'),
      '#empty_option' => $this->t('- Use default -'),
      '#default_value' => isset($defaults['autoplay']) ? \intval($defaults['autoplay']) : NULL,
      '#options' => [
        '0' => $this->t('Disable'),
        '1' => $this->t('Enable'),
      ],
    ];

    $options['pauseOnHover'] = [
      '#type' => 'select',
      '#title' => $this->t('Pause On Hover'),
      '#description' => $this->t('If enabled, the slider will pause autoplay while the user hovers over it.'),
      '#empty_option' => $this->t('- Use default -'),
      '#default_value' => isset($defaults['pauseOnHover']) ? \intval($defaults['pauseOnHover']) : NULL,
      '#options' => [
        '0' => $this->t('Disable'),
        '1' => $this->t('Enable'),
      ],
    ];

    // Remove options that aren't supported in the breakpoint options.
    unset($options['infiniteScrolling']);
    unset($options['enableLiveRegion']);
    unset($options['liveRegionText']);

    foreach ($options as &$element) {
      $element['#states'] = $states;

      if (\array_key_exists('#required', $element)) {
        $element['#required'] = FALSE;
      }
    }

    return $options;
  }

  /**
   * Generate an HTML ID for this carousel.
   *
   * This plugin's base HTML ID is ran through `Html::getUniqueId()` to ensure
   * that each plugin instance has a unique identifier.
   *
   * @return string
   *   The HTML ID.
   */
  protected function getHtmlId(): string {
    if (!isset($this->htmlId)) {
      $this->htmlId = Html::getUniqueId(self::BASE_HTML_ID);
    }

    return $this->htmlId;
  }

  /**
   * Get an array of properly typed option values.
   *
   * @param array $options
   *   The raw options values.
   *
   * @return array
   *   An array of properly typed option values.
   */
  protected function getTypedOptions(array $options): array {
    $options_key_callbacks = [
      'autoplay' => \boolval(...),
      'autoplaySpeed' => \intval(...),
      'enableLiveRegion' => \boolval(...),
      'gap' => \intval(...),
      'itemsPerView' => \intval(...),
      'infiniteScrolling' => \boolval(...),
      'liveRegionText' => \strval(...),
      'pauseOnHover' => \boolval(...),
      'transitionSpeed' => \intval(...),
    ];

    $options = \array_intersect_key($options, $options_key_callbacks);
    $options = \array_filter($options, fn ($value) => $value !== '');

    foreach ($options as $key => &$value) {
      $value = $options_key_callbacks[$key]($value);
    }

    return $options;
  }

  /**
   * Get the settings for the Zodiac library.
   *
   * @return array
   *   The settings for the Zodiac library.
   */
  protected function getZodiacSettings(): array {
    $base_options = $this->options['base_options'] ?? [];
    $breakpoint_group = $this->options['breakpoint_group'] ?? '';
    $breakpoint_options = $this->options['breakpoint_options'] ?? [];

    $breakpoints = $this->breakpointManager->getBreakpointsByGroup($breakpoint_group);
    foreach ($breakpoint_options as $breakpoint_id => $options) {
      $breakpoint_id = "{$breakpoint_group}.{$breakpoint_id}";
      $breakpoint = $breakpoints[$breakpoint_id] ?? NULL;

      // The breakpoint must provide a non-empty media query string.
      if (\is_string($media_query = $breakpoint?->getMediaQuery()) && \strlen($media_query)) {
        $base_options['mediaQueryOptions'][$media_query] = $options;
      }
    }

    return $base_options;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $render = parent::render();
    $render = \reset($render);

    $render['#attributes']['id'] = $this->getHtmlId();
    $render['#attributes']['class'][] = 'zodiac';

    $render['#attached']['library'][] = 'zodiac_style_plugin/behavior';
    $render['#attached']['drupalSettings']['zodiac'][$this->getHtmlId()] = $this->getZodiacSettings();

    return $render;
  }

  /**
   * {@inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state) {
    parent::submitOptionsForm($form, $form_state);

    $base_options = $this->getTypedOptions($form_state->getValue(['style_options', 'base_options'], []));
    $breakpoint_group = $form_state->getValue(['style_options', 'breakpoint_group'], '');
    $breakpoint_options = [];

    if ($breakpoint_group !== '') {
      // Strip the breakpoint group from the breakpoint options hierarchy.
      //
      // This step is necessary to satisfy configuration validation since
      // configuration keys cannot contain period characters.
      $breakpoint_options = $form_state->getValue(['style_options', 'breakpoint_options', $breakpoint_group], []);
      $breakpoint_options = \array_filter(\array_map($this->getTypedOptions(...), $breakpoint_options), \count(...));

      // Clear the selected breakpoint group if no options were set.
      if ($breakpoint_options === []) {
        $breakpoint_group = '';
      }
    }

    // Update the form values after sanitization.
    $form_state->setValue(['style_options', 'base_options'], $base_options);
    $form_state->setValue(['style_options', 'breakpoint_group'], $breakpoint_group);
    $form_state->setValue(['style_options', 'breakpoint_options'], $breakpoint_options);
  }

}
